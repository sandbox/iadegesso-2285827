<?php
/**
 * @file
 * Conversion XML -> AREL for ARgomentiamo module.
 */
 function xmlToArel() {


if(!isset($_GET['token'])){
    header("HTTP/1.1 401 Unauthorized");
	
	exit();
    
}

require_once 'ARELLibrary/arel_xmlhelper.class.php';

if(!empty($_GET['l'])) 

	$position = explode(",", $_GET['l']);
    
else

	trigger_error("user position (l) missing. For testing, please provide a 'l' GET parameter with your request. e.g. pois/search/?l=23.34534,11.56734,0");


//get token passed by url
$token = $_GET['token'];
	
//create the xml start
ArelXMLHelper::start(NULL, "/arel/index.php?token=".$_GET['token'], ArelXMLHelper::TRACKING_GPS);

//start by defining some positions of geo referenced POIs and give those names and thumbnails
$locations = array();

//token and xml data OLD VERSION


$xmlsource = 'http://' . $_SERVER['SERVER_NAME'] . $_GET['basepath'] . 'argomentiamo-view?token=' . $token;
$xmldata = file_get_contents($xmlsource);


if($xmldata != ""){
	$nodes = new SimpleXMLElement($xmldata);
}else{// in case your hosting block the http connection, like for example altervista.org 

//token and xml data
$xmlsource = 'http://' . $_SERVER['SERVER_NAME'].'/Argomentiamo/?q='. 'argomentiamo-view' /*?token=' . $token*/;

$ch = curl_init($xmlsource); 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
$sorgente = curl_exec($ch); 
curl_close($ch);
//$xmldata = highlight_string($sorgente,true);
//var_dump($xmldata);
$nodes = new SimpleXMLElement($sorgente);
}



	foreach ($nodes->node as $obj) {
			$dataCoords = str_replace('Geolocation is', '', $obj->field_geoposition);
			$dataBody = $obj->body;
			$dataTitle = $obj->title;
			$dataDate = $obj->created;
			$dataType = $obj->typo;
			$dataLink = $obj->path;
			
			$locations[] = array($dataCoords, $dataTitle, $dataBody, $dataDate, $dataType, $dataLink);
	}

//display the POIs as defined in the Constructor
foreach($locations as $i => $findPOI)
{	

	//title of the POI
	$title = $findPOI[1];
	$title = preg_replace_callback("/(&#[0-9]+;)/", function($m) { return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); }, $title); 
	$coords = $findPOI[0];
	$body = $findPOI[2];
	$body =  preg_replace_callback("/(&#[0-9]+;)/", function($m) { return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); }, $body); 
	$date = $findPOI[3];
	$type = $findPOI[4];
	$link = $findPOI[5];
	
	//create the POI createLocationBasedPOI( $id, $title, $location, $thumbnail, $icon, $description, $buttons )
	$poi = ArelXMLHelper::createLocationBasedPOI(
		$i,
		$title,
		explode(",", $coords),
		"/resources/thumb.png",
		"/resources/icon.png",
		$body,
		array(
			array("Link", "button",'http://' . $_SERVER['SERVER_NAME'] . $link /*. '?token=' . $token*/),
    ));  
	
	//20000km -> 20'000'000m -> see them all over the world
	$poi->setMaxDistance(1000000);	
	
	//output the POI
	ArelXMLHelper::outputObject($poi);
}				

//end the output
ArelXMLHelper::end();

}

xmlToArel();

	//return 'example page';
	//exit to clear after the call ;)
	exit();
?>
