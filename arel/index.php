<?php
//get the token
$piecesToken = explode("/", $_GET['token']);
$token = $piecesToken[0];
//get the url
$piecesUrl = explode("/", $_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']));
$theUrl = $piecesUrl[0] . "/" . $piecesUrl[1];
?>
<html>
    <head>
    	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    	<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
       	<script type="text/javascript" src="http://dev.junaio.com/arel/js/arel.js"></script>
    	<script type="text/javascript" src="logic_LBS9.js"></script>
		<script>
		function getLocation(action) {
			arel.Scene.getLocation(function(location) {
				var userLat = location.getLatitude();
				var userLng = location.getLongitude();
				var myToken = "<?php echo $token ?>";
              
              // invert redirect whit prova in case you don't use altervista.org

				var redirect = "http://<?php echo $theUrl; ?>" + 
                				"?q=node/add/" + action + "/" +
								"?edit[field_geoposition][und][0][lat]=" + userLat +
								"&edit[field_geoposition][und][0][lng]=" + userLng + 
                                "?token=<?php echo $token ?>";
                

				var prova2 = 'http://<?php echo $theUrl; ?>?token=<?php echo $token ?>#overlay=%3Fq%3Dnode%252Fadd%252Farticle';
                
                var prova = "http://<?php echo $theUrl; ?>/Argomentiamo/" +
								"/node/add/" + action + "/" +
								"?edit[field_geoposition][und][0][lat]=" + userLat +
								"&edit[field_geoposition][und][0][lng]=" + userLng +
								"&token=<?php echo $token ?>";
				
				window.location.assign(redirect);
			}, this);
		}
        function viewBookmark(action) {
        
        	arel.Scene.getLocation(function(location) {
        		var redirect = "http://<?php echo $theUrl; ?>" + 
                				"?q=bookmarks/"+
								
                                "?token=<?php echo $token ?>";
                                
            	window.location.assign(redirect);
        	}, this);
        }
        
        function homePage() {
        
        	arel.Scene.getLocation(function(location) {
        		var redirect = "http://<?php echo $theUrl; ?>" + 
                				"?token=<?php echo $token ?>";
                                
            	window.location.assign(redirect);
        	}, this);
        }
        
        function searchContent() {
        
        var x = document.getElementById("myInput").value;
        	arel.Scene.getLocation(function(location) {
            
            	
        		var redirect = "http://<?php echo $theUrl; ?>" +
                				"?q=search/node/" + x;
                                
                                
            	window.location.assign(redirect);
        	}, this);
        }
		</script>
    	<link href="style.css" rel="stylesheet" type="text/css" />
    	<title>ARgomentiamo</title>
    </head>
	<body>
		<div class="filteroptions">
			<div class="filterbuttonArea">	
				<a href="#" class="filterButton">Actions</a>
			</div>
			
			<div class="filterOptionsInner">
				<div class="dropdown">
					<p><button onclick="getLocation('article')">Add Article</button></p>
                    <p><button onclick="viewBookmark('bookmark')">My Bookmarks</button></p>
                    <p><button onclick="homePage()">Home Page</button></p>
                    <p><text>Search Content:</text></p>
                   	<p><input type="text" id="myInput"  oninput=""></p>
                    <p><button onclick="searchContent()">Search</button></p>
                    
				</div>
				
		</div>		
	</body>            
</html>
