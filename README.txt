# ARgomentiamo README.txt

Description

ARgomentiamo is a module to publish geolocalized content on the Internet (for its display on a AR browser ***) and to accept its modification from this app. ARgomentiamo means "let's discuss" in Italian. This an ongoing project that is initially conceived to display information on active petitions, polls and complaints submitted through the Condiviviamo platform, and accept the user contribution.

Conceived within the Condiviviamo Project developed at the University of Milano-Bicocca and supervised by Federico Cabitza, PhD.

Dependencies
	.Geolocation Field
	.Prepopulate
	.Services
	.Token authentication
	.Views
	.Views Datasource
        .Flag
	.Social Buttons
	.Get Directions
	
Other dependencies
	.Junaio - Augmented Reality Browser (http://www.junaio.com/)