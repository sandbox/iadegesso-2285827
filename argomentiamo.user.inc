<?php
/**
 * @file
 * User tab for ARgomentiamo.
 */

/**
* Implements Form Builder user_profile_form()
*/
function argomentiamo_user_profile_form($form, &$form_state, $account) {
	drupal_set_title($account->name);
	
	/**
	* Get user id and user channel
	*/
	global $user;
	$userID = $user->uid;    

	$userChannel = db_select('argomentiamo_channels','n') 
				->fields('n',array('channel','uid'))
				->condition('n.uid', $userID, '=')
				->execute()
				->fetchAssoc();

	$userChannel = $userChannel['channel'];	
	
	/**
	 * Page markup: description	=> a simple description for the QR code.
	 */
	$form['description'] = array(
		'#markup' => t('<p>Use the Junaio App to recognize the QR Code.<br><a href="http://www.junaio.com/download/">http://www.junaio.com/download/</a>
			<br>

			

			<img src="http://phytonscoop.altervista.org/Argomentiamo/sites/all/modules/argomentiamo/qrcode" />
			</p><br>'),


	);


	
	/**
	 * Page markup: image	=> the QR code image generated with goqr.me API .
	 */
	

	$qrCodeData = 'http://www.junaio.com/downloadnow/index/channel/' . $userChannel;
	$image_options = array(

		

		/*'path' => 'https://api.qrserver.com/v1/create-qr-code/?data='.urlencode($qrCodeData).'&amp;size=150x150', 
		'alt' => $userChannel,
		'title' => $userChannel,
		'width' => '150px',
		'height' => '150px',
		'attributes' => array('class' => 'some-img', 'id' => 'my-img'
		
			)),

		*/
	);
	$image = theme('image', $image_options);
	$form['image'] = array(
	'#markup' => $image,
	);
	
	return $form;
	
}
